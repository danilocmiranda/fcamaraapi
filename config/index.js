/**
 * Basic configuration object
 */
module.exports = {
  auth: {
    secret: 'Algo secreto!'
  },
  database: {
    local: 'mongodb://localhost/fcamara',
    mLab: '' // if you want to use mLab for example
  }
};

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

var ProductSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('Product', ProductSchema);

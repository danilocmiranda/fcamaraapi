var express = require('express');
var router = express.Router();
let passport = require('passport');
let jwt = require('jsonwebtoken');
var Product = require('../models/Product');
let config = require('../config');

router.get('/', function(req, res) {
  Product.find({}, function(err, products) {
    res.json(products);
  });
});

router.post('/save', function(req, res) {
  if (!req.body.name || !req.body.description) {
    res.json({
      success: false,
      message: 'Informe nome e descrição.'
    });
  } else {
    let newProduct = new Product({
      name: req.body.name,
      description: req.body.description
    });

    newProduct.save(function(err) {
      if (err) {
        return res.json({
          success: false,
          message: 'Este produto já existe.'
        });
      }
      res.json({
        success: true,
        message: 'Produto criado com sucesso.'
      });
    });
  }
});

module.exports = router;

var express = require('express');
var router = express.Router();
let passport = require('passport');
let jwt = require('jsonwebtoken');
var User = require('../models/User');
let config = require('../config');

router.post('/register', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({
      success: false,
      message: 'Informe email e senha.'
    });
  } else {
    let newUser = new User({
      email: req.body.email,
      password: req.body.password
    });

    // Attempt to save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({
          success: false,
          message: 'Este email já existe.'
        });
      }
      res.json({
        success: true,
        message: 'Usuário criado com sucesso.'
      });
    });
  }
});

router.get('/', function(req, res) {
  User.find({}, function(err, users) {
    res.json(users);
  });
});

// Authenticate the user and get a JSON Web Token to include in the header of future requests.
router.post('/auth', (req, res) => {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({
        success: false,
        message: 'Falha na Autenticação. Usuário não encontrado.'
      });
    } else {
      // Check if password matches
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          // Create token if the password matched and no error was thrown
          var token = jwt.sign(user, config.auth.secret, {
            expiresIn: "1m"
          });
          res.json({
            success: true,
            message: 'Autenticado com Sucesso',
            token
          });
        } else {
          res.send({
            success: false,
            message: 'Falha na Autenticação. Senha não confere.'
          });
        }
      });
    }
  });
});

module.exports = router;
